import React, { useState } from 'react'
import Select from 'react-select'
import { customStylesLang } from 'StyledComponents/select'
import i18n from 'i18n'

interface LngListItem {
    value: string
    label: string
}

const LangModule: React.FC = () => {
    const lang: Array<LngListItem> = [
        { value: 'en', label: 'EN' },
        { value: 'uk', label: 'UA' },
        { value: 'ru', label: 'RU' },
    ]

    const currentlng: LngListItem | undefined = lang.find((lng) => lng.value === i18n.language)

    const [lng, setlng] = useState<LngListItem | undefined>(currentlng)

    const onChangeLng = (data: LngListItem): void => {
        setlng(data)
        i18n.changeLanguage(data.value)
        localStorage.setItem('lng', data.value)
    }

    return (
        <div style={{ paddingRight: '10px' }}>
            <Select
                className="basic-single"
                options={lang}
                styles={customStylesLang}
                value={lng}
                onChange={onChangeLng}
            />
        </div>
    )
}

export default LangModule
