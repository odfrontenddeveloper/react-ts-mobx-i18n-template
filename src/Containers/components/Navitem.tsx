import React from 'react'
import { Link } from 'react-router-dom'
import { NavItem, NavItemContainer } from 'StyledComponents/navmenu'

interface NavItemProps {
    to?: string,
    text?: string,
    selected?: boolean 
}

const Navitem: React.FC<NavItemProps> = ({ to = '/', text = '', selected }) => (
    <NavItemContainer>
        <Link to={to}>
            <NavItem selected={!!selected}>{text}</NavItem>
        </Link>
    </NavItemContainer>
)

export default Navitem