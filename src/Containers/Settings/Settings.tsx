import React from 'react'
import { MenuTab } from 'StyledComponents/page'

const Settings: React.FC = () => {
    return (
        <MenuTab>
            <div>Settings</div>
        </MenuTab>
    )
}

export default Settings
