import React from 'react'
import Select from 'react-select'
import { Route, Switch, useLocation } from 'react-router'
import { Link } from 'react-router-dom'
import {
    AppName,
    HeaderButtonContainer,
    Information,
    PageContainer,
    PageHeader,
    WhiteBlanck,
} from 'StyledComponents/page'
import { NavMenu } from 'StyledComponents/navmenu'
import { FormButton } from 'StyledComponents/form'
import Settings from 'Containers/Settings/Settings'
import Navitem from 'Containers/components/Navitem'
import Admins from 'Containers/Admins/Admins'
import LangModule from 'Containers/components/LangModule'
import { customStylesOrganization } from 'StyledComponents/select'

const options = [
    { value: 1, label: 'Академия связи' },
    { value: 2, label: 'Мечникова' },
    { value: 3, label: 'Политех' },
]

interface Location {
    pathname: string
}

const OwnerPage: React.FC = () => {
    const location: Location = useLocation()

    const currentLocation: string = location.pathname.split('/')[3]

    return (
        <PageContainer>
            <PageHeader>
                <Information>
                    <AppName>Study link</AppName>
                    <LangModule />
                    <Select
                        className="basic-single"
                        placeholder="Выберите организацию"
                        options={options}
                        styles={customStylesOrganization}
                    />
                    <HeaderButtonContainer>
                        <Link to="/login/auth">
                            <FormButton type="button" themed>
                                Выйти
                            </FormButton>
                        </Link>
                    </HeaderButtonContainer>
                </Information>
                <NavMenu>
                    <Navitem to="/home/owner/home" text="Домашняя страница" selected={currentLocation === 'home'} />
                    <Navitem to="/home/owner/admins" text="Администраторы" selected={currentLocation === 'admins'} />
                    <Navitem to="/home/owner/subjects" text="Предметы" selected={currentLocation === 'subjects'} />
                    <Navitem to="/home/owner/courses" text="Курсы" selected={currentLocation === 'courses'} />
                    <Navitem to="/home/owner/groups" text="Группы" selected={currentLocation === 'groups'} />
                    <Navitem to="/home/owner/settings" text="Настройки" selected={currentLocation === 'settings'} />
                </NavMenu>
            </PageHeader>
            <WhiteBlanck>
                <Switch>
                    <Route path="/home/owner/admins" component={Admins} />
                    <Route path="/home/owner/settings" component={Settings} />
                </Switch>
            </WhiteBlanck>
        </PageContainer>
    )
}

export default OwnerPage
