import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import Form from 'BuildComponents/Form'
import { LoginContainer } from './style'

const NewUser: React.FC = () => {

    const { t } = useTranslation()

    const onSubmit = (values) => {}

    return (
        <LoginContainer>
            <Form
                handleSubmit={onSubmit}
                title={t('newuser.title')}
                direction={{
                    body: 'column',
                    footer: 'column',
                }}
                fields={[
                    {
                        label: t('newuser.email.label'),
                        name: 'email',
                        type: 'text',
                        placeholder: t('newuser.email.placeholder'),
                        validate: ['required', 'email'],
                    },
                    {
                        label: t('newuser.firstname.label'),
                        name: 'firstname',
                        type: 'text',
                        placeholder: t('newuser.firstname.placeholder'),
                        validate: ['required', 'minlength5', 'maxlength45'],
                    },
                    {
                        label: t('newuser.middlename.label'),
                        name: 'middlename',
                        type: 'text',
                        placeholder: t('newuser.middlename.placeholder'),
                        validate: ['required', 'minlength5', 'maxlength45'],
                    },
                    {
                        label: t('newuser.lastname.label'),
                        name: 'lastname',
                        type: 'text',
                        placeholder: t('newuser.lastname.placeholder'),
                        validate: ['required', 'minlength5', 'maxlength45'],
                    },
                    {
                        label: t('newuser.password.label'),
                        name: 'password',
                        type: 'password',
                        placeholder: t('newuser.password.placeholder'),
                        validate: ['required', 'minlength5', 'maxlength45'],
                    },
                    {
                        label: t('newuser.repeatpassword.label'),
                        name: 'repeatpassword',
                        type: 'password',
                        placeholder: t('newuser.repeatpassword.placeholder'),
                        validate: ['required', 'minlength5', 'maxlength45'],
                    },
                ]}
                submitButtonOptions={{
                    themed: true,
                    color: 'primary',
                    content: t('newuser.buttons.signup'),
                }}
                additionalButtons={[
                    {
                        type: 'button',
                        color: 'link',
                        content: <Link to="/login/auth">{t('newuser.buttons.login')}</Link>,
                    },
                ]}
            />
        </LoginContainer>
    )
}

export default NewUser
