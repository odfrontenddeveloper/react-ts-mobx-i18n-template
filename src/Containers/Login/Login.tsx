import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import Form from 'BuildComponents/Form'
import { LoginContainer } from './style'

const Login: React.FC = () => {
    const { t } = useTranslation()

    const onSubmit = (values): void => {
        console.log(values)
    }

    return (
        <LoginContainer>
            <Form
                handleSubmit={onSubmit}
                title={t('login.title')}
                direction={{
                    body: 'column',
                    footer: 'column',
                }}
                fields={[
                    {
                        label: t('login.email.label'),
                        name: 'email',
                        type: 'text',
                        placeholder: t('login.email.placeholder'),
                        validate: ['required', 'email'],
                    },
                    {
                        label: t('login.password.label'),
                        name: 'password',
                        type: 'password',
                        placeholder: t('login.password.placeholder'),
                        validate: ['required', 'minlength5', 'maxlength45'],
                    },
                ]}
                submitButtonOptions={{
                    themed: true,
                    content: t('login.buttons.login'),
                }}
                additionalButtons={[
                    {
                        type: 'button',
                        color: 'link',
                        content: <Link to="/login/new">{t('login.buttons.createnew')}</Link>,
                    },
                    {
                        type: 'button',
                        color: 'link',
                        content: <Link to="/login/new">{t('login.buttons.forgot')}</Link>,
                    },
                ]}
            />
        </LoginContainer>
    )
}

export default Login
