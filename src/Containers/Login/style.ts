import styled from "styled-components";

export const LoginContainer = styled.div`
    width: 100%;
    height: 100%;
    padding: 5% 5px 5% 5px;
    overflow-y: auto;
    background-image: ${props => props.theme.mainbg};
    display: flex;
    align-items: flex-start;
    justify-content: center;
`