import React, { useState } from 'react'
import DataTable from 'BuildComponents/DataTable'
import { FormButton } from 'StyledComponents/form'
import { MenuTab } from 'StyledComponents/page'
import modalStore from 'store/modal'

const Admins: React.FC = () => {
    const [data /*, setdata*/] = useState(
        Array(125)
            .fill(null)
            .map((item, i) => ({
                data: {
                    id: 1,
                    firstname: 'Vladislav',
                    middlename: 'Igorevich',
                    lastname: 'Bogdashev',
                    email: '0933931313q@gmail.com',
                    phoneNumber: '+3800933931313',
                    control: i + 1,
                },
                buttons: [
                    {
                        color: 'secondary',
                        content: 'Сессии',
                        notifications: i + 1 === 3 ? 35 : 0,
                    },
                    {
                        color: 'primary',
                        content: 'Редактировать',
                        onClick: (element, index) => alert(index),
                    },
                    {
                        color: 'danger',
                        content: 'Удалить',
                        onClick: (element, index) =>
                            modalStore.callDeleteModal({
                                text: 'Are you sure you want to delete this item?',
                                onConfirm: () => {
                                    console.log(element, index)
                                    return 1
                                },
                            }),
                    },
                ],
            }))
    )

    return (
        <MenuTab>
            <div style={{ padding: 7 }}>
                <FormButton themed>Cоздать пользователя</FormButton>
            </div>
            <DataTable
                tablename="Список пользователей"
                headers={[
                    { label: 'First name', key: 'firstname' },
                    { label: 'Middle name', key: 'middlename' },
                    { label: 'Last name', key: 'lastname' },
                    { label: 'Email', key: 'email' },
                    { label: 'Phone number', key: 'phoneNumber' },
                    { label: 'control', key: 'control' },
                ]}
                data={data}
                paginate={20}
                withIndex
                withPaginateRange
                withPaginateButtons
            />
        </MenuTab>
    )
}

export default Admins
