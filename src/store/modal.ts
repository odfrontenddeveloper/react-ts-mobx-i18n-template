import { makeAutoObservable, action } from 'mobx'

interface DeleteModalStore {
    deleteModal: {
        text?: string
        onConfirm?: () => any
    }
    isDeleteModalOpen: boolean
}

interface ModalOptions {
    text: string
    onConfirm: () => any
}

class DeleteModal {
    modal: DeleteModalStore = {
        deleteModal: {},
        isDeleteModalOpen: false,
    }

    constructor() {
        makeAutoObservable(this)
        this.callDeleteModal = this.callDeleteModal.bind(this)
        this.closeDeleteModal = this.closeDeleteModal.bind(this)
    }

    @action callDeleteModal({ text, onConfirm }: ModalOptions) {
        this.modal = {
            deleteModal: {
                text,
                onConfirm,
            },
            isDeleteModalOpen: true,
        }
    }

    @action closeDeleteModal() {
        this.modal = {
            deleteModal: {},
            isDeleteModalOpen: false,
        }
    }
}

const modalStore = new DeleteModal()

export default modalStore
