import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { FormButton } from 'StyledComponents/form'
import {
    Index,
    IndexContainer,
    Table,
    TBody,
    Td,
    TdContentHeader,
    TdContentValue,
    TdContentContainer,
    TdButtonContainer,
    Tr,
    TableContainer,
    TablePage,
    TableHeader,
    PaginateContainer,
    Wrapper,
    Range,
    PaginateControl,
    PaginateControlLabel,
    PaginateButton,
    SearchContainer,
    SearchContainerSelect,
    SearchContainerText,
    SearchContainerInput,
    SortContainerSelect,
} from 'StyledComponents/table'
import { useTranslation } from 'react-i18next'

interface Buttons {
    color?: string
    content?: string
    onClick?: (f: object, s: number) => void
    notifications?: number | string
}

interface DataStructure {
    data: object
    buttons?: Array<Buttons>
}

interface HeaderStructure {
    key: string,
    label: string
}

interface SortingInterface {
    type?: string,
    value?: string
}

const RenderTdIndexItem: React.FC<{ i: number }> = ({ i }) => (
    <Td themed>
        <IndexContainer themed>
            <Index>{i}</Index>
        </IndexContainer>
    </Td>
)

const RenderTdContentItem: React.FC<{
    header: string
    content: string
    width: number
}> = ({ header = '', content = '', width = 1 }) => (
    <Td width={`${100 / width}%`}>
        <TdContentContainer>
            <TdContentHeader>{header}</TdContentHeader>
            <TdContentValue>{content}</TdContentValue>
        </TdContentContainer>
    </Td>
)

const RenderTdButtonItem: React.FC<{ content?: any }> = ({ content = '' }) => (
    <Td buttonContainer>
        <TdButtonContainer>{content}</TdButtonContainer>
    </Td>
)

const DataTable: React.FC<{
    withIndex?: boolean
    withPaginateRange?: boolean
    withPaginateButtons?: boolean
    headers: Array<HeaderStructure>
    data: Array<DataStructure>
    tablename?: string
    paginate?: number
}> = ({
    withIndex,
    withPaginateRange,
    withPaginateButtons,
    headers = [],
    data = [],
    tablename = '',
    paginate = 0,
}) => {
    const { t } = useTranslation()

    const [paginateValue, setpaginate] = useState<number>(paginate)
    const [calcZeros, setCalcZeros] = useState<string>('')
    const [searchValue, setsearchValue] = useState<string>('')
    const [sorting, setsorting] = useState<SortingInterface>({})

    const maxValue: number =
        data.filter((item: { data: object }, i: number) => {
            const searchString: string = headers
                .map((header: HeaderStructure): string => item.data[header.key])
                .join(' ')
            return searchString.indexOf(searchValue) !== -1
        }).length %
            paginate ===
        0
            ? data.filter((item: { data: object }, i) => {
                  const searchString = headers.map((header: { key: string }) => item.data[header.key]).join(' ')
                  return searchString.indexOf(searchValue) !== -1
              }).length
            : data.filter((item: { data: object }, i) => {
                  const searchString = headers.map((header: { key: string }) => item.data[header.key]).join(' ')
                  return searchString.indexOf(searchValue) !== -1
              }).length -
              (data.filter((item: { data: object }, i) => {
                  const searchString = headers.map((header: { key: string }) => item.data[header.key]).join(' ')
                  return searchString.indexOf(searchValue) !== -1
              }).length %
                  paginate) +
              paginate

    useEffect(() => {
        !!paginate && setpaginate(paginate)
    }, [paginate, data])

    useEffect(() => {
        !!paginate && setpaginate(paginate)
    }, [paginate, setpaginate, searchValue])

    useEffect(() => {
        if (!!paginate) {
            const length = String(maxValue / paginate).length - String(paginateValue / paginate).length
            length >= 0 && setCalcZeros(Array(length).fill('0').join(''))
        }
    }, [paginate, paginateValue, maxValue, setCalcZeros])

    const handleChangeSorting = (e: React.ChangeEvent<HTMLSelectElement>) => {
        if (!!e.target.value) {
            setsorting({
                type: !!Object.keys(sorting).length ? sorting.type : 'ascending',
                value: e.target.value,
            })
        } else {
            setsorting({})
        }
    }

    const handleChangeSortType = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setsorting({
            type: e.target.value,
            value: sorting.value,
        })
    }

    const getSortedData = (data) => {
        if (!!Object.keys(sorting).length) {
            let sorteddata: Array<object> = _.sortBy(data, [`data.${sorting.value}`])
            if (sorting.type === 'ascending') return sorteddata
            return sorteddata.reverse()
        } else {
            return data
        }
    }

    const calcThumbWidth: number = !!paginate
        ? 100 / (maxValue / paginate) >= 25
            ? 100 / (maxValue / paginate)
            : 25
        : 25

    return (
        <TablePage>
            <TableHeader>{tablename}</TableHeader>
            {!!paginateValue && (
                <div>
                    <PaginateContainer>
                        <PaginateControl>
                            {withPaginateButtons && (
                                <PaginateButton
                                    onClick={() => {
                                        paginateValue !== paginate &&
                                            paginateValue - paginate !== 0 &&
                                            setpaginate(paginateValue - paginate)
                                    }}
                                    leftButton
                                >
                                    {'<'}
                                </PaginateButton>
                            )}
                            <PaginateControlLabel leftRadius={!withPaginateButtons}>{`${calcZeros}${
                                paginateValue / paginate
                            }/${maxValue / paginate}`}</PaginateControlLabel>
                            {withPaginateButtons && (
                                <PaginateButton
                                    onClick={() => {
                                        ;+paginateValue + +paginate <= +maxValue &&
                                            setpaginate(+paginateValue + +paginate)
                                    }}
                                    rightButton
                                >
                                    {'>'}
                                </PaginateButton>
                            )}
                        </PaginateControl>
                        {!!withPaginateRange && !!paginateValue && (
                            <Wrapper>
                                <Range
                                    type="range"
                                    thumbWidth={`${calcThumbWidth}%`}
                                    min={paginate}
                                    max={maxValue}
                                    step={paginate}
                                    value={paginateValue}
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setpaginate(Number(e.target.value))}
                                />
                            </Wrapper>
                        )}
                        <SearchContainer>
                            <SearchContainerText>{t('datatable.search.title')}</SearchContainerText>
                            <SearchContainerInput
                                type="text"
                                placeholder={t('datatable.search.enterstr')}
                                value={searchValue}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setsearchValue(e.target.value)}
                            />
                        </SearchContainer>
                        <SearchContainer>
                            <SearchContainerText>{t('datatable.sort.title')}</SearchContainerText>
                            {!!Object.keys(sorting).length && (
                                <SearchContainerSelect value={sorting.type} onChange={handleChangeSortType}>
                                    <option value={'ascending'}>{t('datatable.sort.ascending')}</option>
                                    <option value={'descending'}>{t('datatable.sort.descending')}</option>
                                </SearchContainerSelect>
                            )}
                            <SortContainerSelect value={!!Object.keys(sorting).length && sorting.value} onChange={handleChangeSorting}>
                                <option value={''}></option>
                                {headers.map((item: { key: string; label: string }, i: number) => (
                                    <option key={'sort-' + i} value={item.key}>
                                        {item.label}
                                    </option>
                                ))}
                            </SortContainerSelect>
                        </SearchContainer>
                    </PaginateContainer>
                </div>
            )}
            <TableContainer>
                <Table>
                    <TBody>
                        {getSortedData(data)
                            .filter((item: { data: object }, i: number): boolean => {
                                const searchString: string = headers
                                    .map((header: { key: string }) => item.data[header.key])
                                    .join(' ')
                                return searchString.indexOf(searchValue) !== -1
                            })
                            .filter((item: object, i: number): boolean =>
                                (!!paginate && paginate !== 0) ? i >= paginateValue - paginate && i < paginateValue : true
                            )
                            .map((item: { data: object; buttons: Array<Buttons> }, i: number) => (
                                <Tr key={i}>
                                    {!!withIndex && (
                                        <RenderTdIndexItem i={paginate ? +i + 1 + +paginateValue - +paginate : i + 1} />
                                    )}
                                    {headers.map((header: HeaderStructure, index: number) => (
                                        <RenderTdContentItem
                                            width={headers.length}
                                            header={(!!header && header.label) || ''}
                                            content={(!!item && !!header && item.data[header.key]) || ''}
                                            key={i + '-' + index}
                                        />
                                    ))}
                                    {item.buttons.map((button: Buttons, index: number) => {
                                        const onClickFunc = !!button && !!button.onClick ? button.onClick : () => {}

                                        return (
                                            <RenderTdButtonItem
                                                key={'button-' + index}
                                                content={
                                                    <FormButton
                                                        type="button"
                                                        color={(!!button && button.color) || 'secondary'}
                                                        onClick={() => onClickFunc(item.data, i)}
                                                        notifications={button.notifications}
                                                    >
                                                        {(!!button && button.content) || ''}
                                                    </FormButton>
                                                }
                                            />
                                        )
                                    })}
                                </Tr>
                            ))}
                    </TBody>
                </Table>
            </TableContainer>
        </TablePage>
    )
}

export default DataTable
