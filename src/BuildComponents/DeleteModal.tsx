import React, { useState } from 'react'
import { observer } from 'mobx-react'
import Modal from 'BuildComponents/Modal'
import Form from 'BuildComponents/Form'
import Spinner from 'StyledComponents/spinner.gif'
import { Waiting } from 'StyledComponents/modal'
import modalStore from 'store/modal'

const DeleteModal: React.FC = observer(() => {
    const [isloading, setisloading] = useState<boolean>(false)

    if (!modalStore.modal.isDeleteModalOpen) return null

    const func = async (): Promise<any> => {
        if (!!modalStore.modal.deleteModal.onConfirm) {
            const res = await modalStore.modal.deleteModal.onConfirm()
            return res
        }
        return false
    }

    const handleSubmit = (): void => {
        setisloading(true)
        func().then((res) => {
            setisloading(false)
            modalStore.closeDeleteModal()
        })
    }

    const modalContent = (): React.ReactNode => {
        if (!isloading) {
            return (
                <Form
                    handleSubmit={handleSubmit}
                    title={modalStore?.modal?.deleteModal?.text || ''}
                    direction={{
                        footer: 'row',
                    }}
                    submitButtonOptions={{
                        color: 'danger',
                        content: 'Delete',
                    }}
                    additionalButtons={[
                        {
                            type: 'button',
                            color: 'secondary',
                            content: 'Cancel',
                            onClick: modalStore.closeDeleteModal,
                        },
                    ]}
                />
            )
        } else {
            return (
                <Waiting>
                    <div>Loading...</div>
                    <img src={Spinner} style={{ width: '60px' }} alt='' />
                </Waiting>
            )
        }
    }

    return <Modal onClickOutside={modalStore.closeDeleteModal} content={modalContent()} />
})

export default DeleteModal
