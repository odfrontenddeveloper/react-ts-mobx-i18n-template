import React from 'react'
import ReactDOM from 'react-dom'
import { MdClose } from 'react-icons/md'
import { ModalContainer, ModalClickOutside, ModalContent, CloseModalButton } from 'StyledComponents/modal'

interface ModalInterface {
    content: React.ReactNode,
    onClickOutside: () => any
}

const Modal: React.FC<ModalInterface> = ({ content, onClickOutside }) => {
    const dist: HTMLElement | null = document.getElementById('root')

    if(!dist) return null

    return ReactDOM.createPortal(
        <ModalContainer>
            <ModalClickOutside onClick={onClickOutside}></ModalClickOutside>
            <ModalContent>
                <CloseModalButton type="button" onClick={onClickOutside}>
                    <MdClose />
                </CloseModalButton>
                {content}
            </ModalContent>
        </ModalContainer>,
        dist
    )
}

export default Modal
