import React, { useEffect, useMemo, useState } from 'react'
import {
    ErrorsContainer,
    FieldContainer,
    FieldLabel,
    Form as StyledForm,
    FormBody,
    FormButton,
    FormButtonContainer,
    FormFooter,
    FormHeader,
    TextField,
} from 'StyledComponents/form'
import validations from 'helpers/validation'
import { useTranslation } from 'react-i18next'

interface SubmitButtonOptions {
    themed?: boolean,
    content?: any,
    color?: string
}

interface AdditionalButtons {
    themed?: boolean,
    type?: string,
    color?: string,
    content?: any,
    onClick?: () => void
}

interface DirectionType {
    body?: string
    footer?: string
}

interface Field {
    label: string,
    name: string,
    placeholder?: string,
    disabled?: boolean,
    type: string,
    validate?: Array<string>
}

interface FieldWithValue {
    label: string,
    name: string,
    placeholder?: string,
    disabled?: boolean,
    type: string,
    validate?: Array<string>,
    value: string
}

interface FormInterface {
    title: string,
    fields?: Array<Field>,
    handleSubmit: (value: any) => void,
    direction?: DirectionType,
    initialValues?: object,
    additionalButtons?: Array<AdditionalButtons>
    submitButtonOptions?: SubmitButtonOptions
}

const Form: React.FC<FormInterface> = (props) => {
    const { t } = useTranslation()

    const {
        title,
        fields = [],
        handleSubmit = (value) => {},
        direction,
        initialValues = null,
        additionalButtons = [],
        submitButtonOptions,
    } = props

    const [errorslist, seterrors] = useState<object>([])

    const [fieldslist, changefields] = useState<Array<FieldWithValue>>(
        fields.map((field: Field) => ({
            ...field,
            value: !!initialValues && initialValues[field.name] ? initialValues[field.name] : '',
        }))
    )

    const memoizedFields = useMemo(() => fields, [fields])
    const memoizedInitialValues = useMemo(() => initialValues, [initialValues])

    useEffect(() => {
        if (memoizedFields.length) {
            seterrors([])
            changefields(
                memoizedFields.map((field) => ({
                    ...field,
                    value:
                        !!memoizedInitialValues && memoizedInitialValues[field.name]
                            ? memoizedInitialValues[field.name]
                            : '',
                }))
            )
        }
    }, [memoizedInitialValues, memoizedFields])

    const handleChange = (e, fieldname) => {
        const prevFormState: Array<FieldWithValue> = fieldslist
        const errors: object = { ...errorslist }
        if (errors[fieldname]) {
            delete errors[fieldname]
        }
        seterrors(errors)
        const nextFormState: Array<FieldWithValue> = prevFormState.map((field) => {
            if (field.name === fieldname) {
                field.value = e.target.value
            }
            return field
        })
        changefields(nextFormState)
    }

    const onsubmit = (e) => {
        e.preventDefault()
        const res = {}
        const errors = {}
        fieldslist.forEach((item: FieldWithValue) => {
            if (item.validate) {
                const hasErrors = item.validate.map((func) =>
                    !!validations[func] ? validations[func](item.value) : false
                )
                if (hasErrors.some((item) => !!item)) {
                    errors[item.name] = hasErrors.filter((error) => !!error)
                }
            }
            res[item.name] = item.value
        })
        if (!Object.keys(errors).length) {
            handleSubmit(res)
        } else {
            seterrors(errors)
        }
    }

    return (
        <StyledForm onSubmit={onsubmit}>
            <FormHeader>{title}</FormHeader>
            {!!fieldslist.length && (
                <FormBody direction={direction?.body}>
                    {fieldslist.map((field: FieldWithValue, i: number) => {
                        return (
                            <FieldContainer key={field.name + ' - ' + i}>
                                <FieldLabel htmlFor={field.name}>{field.label}</FieldLabel>
                                <TextField
                                    error={!!errorslist[field.name]?.length}
                                    type={field.type || 'text'}
                                    id={field.name}
                                    placeholder={field.placeholder}
                                    name={field.name}
                                    value={field.value}
                                    onChange={(e) => handleChange(e, field.name)}
                                    disabled={!!field.disabled}
                                />
                                {!!errorslist[field.name]?.length &&
                                    errorslist[field.name].map((error: string, i: number) => (
                                        <ErrorsContainer key={[field.name, i].join('-')}>
                                            {t(`validation.${error}`)}
                                        </ErrorsContainer>
                                    ))}
                            </FieldContainer>
                        )
                    })}
                </FormBody>
            )}
            <FormFooter direction={direction?.footer}>
                <FormButtonContainer>
                    <FormButton
                        type="submit"
                        color={submitButtonOptions ? submitButtonOptions.color : 'primary'}
                        themed={submitButtonOptions && !!submitButtonOptions.themed}
                        maxWidth
                    >
                        {submitButtonOptions ? submitButtonOptions.content : 'Submit'}
                    </FormButton>
                </FormButtonContainer>
                {additionalButtons.map((item: AdditionalButtons, i: number) => {
                    const { onClick = () => {} } = item
                    return (
                        <FormButtonContainer key={i}>
                            <FormButton
                                type={item.type || 'button'}
                                color={item.color || 'primary'}
                                onClick={onClick}
                                themed={!!item.themed}
                                maxWidth
                            >
                                {item.content}
                            </FormButton>
                        </FormButtonContainer>
                    )
                })}
            </FormFooter>
        </StyledForm>
    )
}

export default Form
