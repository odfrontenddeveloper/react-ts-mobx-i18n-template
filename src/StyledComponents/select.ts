import { borderColor, white } from 'StyledComponents/colors'

const control = (provided, state) => ({
    ...provided,
    background: white,
    borderColor: borderColor,
    minHeight: '30px',
    height: '30px',
    width: '300px',
    boxShadow: state.isFocused ? null : null,
    outline: '0',
})

const controlLang = (provided, state) => ({
    ...provided,
    background: white,
    borderColor: borderColor,
    minHeight: '30px',
    height: '30px',
    width: '80px',
    boxShadow: state.isFocused ? null : null,
    outline: '0',
})

const valueContainer = (provided, state) => ({
    ...provided,
    height: '30px',
    padding: '0 6px',
})

const input = (provided, state) => ({
    ...provided,
    margin: '0px',
    outline: '0',
})

export const customStylesOrganization = {
    control,
    valueContainer,
    input,
    indicatorSeparator: (state) => ({
        display: 'none',
    }),
    indicatorsContainer: (provided, state) => ({
        ...provided,
        height: '30px',
    }),
}

export const customStylesLang = {
    control: controlLang,
    valueContainer,
    input,
    indicatorSeparator: (state) => ({
        display: 'none',
    }),
    indicatorsContainer: (provided, state) => ({
        ...provided,
        height: '30px',
    }),
}