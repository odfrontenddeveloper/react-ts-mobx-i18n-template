interface ButtonStates {
    default: string,
    hover: string,
    active: string
}

interface ButtonStylesSchema {
    primary: ButtonStates,
    success: ButtonStates,
    danger: ButtonStates,
    secondary: ButtonStates,
    link: ButtonStates,
}

export const button: ButtonStylesSchema = {
    primary: {
        default: '#0060cf',
        hover: '#004ca3',
        active: '#00397a'
    },
    success: {
        default: '#00bd52',
        hover: '#009641',
        active: '#00612a'
    },
    danger: {
        default: '#d62d02',
        hover: '#871b00',
        active: '#6e1600'
    },
    secondary: {
        default: '#ababab',
        hover: '#858585',
        active: '#616161'
    },
    link: {
        default: '#ffffff',
        hover: '#ffffff',
        active: '#ffffff'
    }
}

export const borderColor: string = '#c4cace'
export const dangerColor: string = '#f55d42'

export const pagecolor: string = '#ededed'
export const lightgrey: string = '#e3e3e3'
export const grey: string = '#c2c2c2'
export const white: string = '#ffffff'
export const dark: string = '#2b2b2b'

export const transition: string = 'all 0.2s ease'
export const fontfamily: string = 'Roboto, sans-serif'
export const tableHeaderColor: string = 'rgba(0, 0, 0, 0.65)'