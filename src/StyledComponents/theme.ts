export const blueTheme = {
    defaultColor: '#5eacff',
    defaultColorOpacity: 'rgba(94, 172, 255, 0.2)',
    mainbg: 'linear-gradient(to top right, #f5f5f5, rgb(107, 183, 255))',
    themedButton: {
        default: '#5eacff',
        hover: '#3881cf',
        active: '#2666ab'
    }
}

export const purpleTheme = {
    defaultColor: '#8c4ac7',
    defaultColorOpacity: 'rgba(141, 74, 199, 0.2)',
    mainbg: 'linear-gradient(to top right, #f5f5f5, #8c4ac7)',
    themedButton: {
        default: '#8c4ac7',
        hover: '#5e2e87',
        active: '#42205e'
    }
}

export const greenTheme = {
    defaultColor: '#7cbf00',
    defaultColorOpacity: 'rgba(124, 191, 0, 0.2)',
    mainbg: 'linear-gradient(to top right, #f5f5f5, #7cbf00)',
    themedButton: {
        default: '#00bd52',
        hover: '#009641',
        active: '#00612a'
    }
}

export const cyanTheme = {
    defaultColor: '#00d6d6',
    defaultColorOpacity: 'rgba(0, 214, 214, 0.2)',
    mainbg: 'linear-gradient(to top right, #f5f5f5, #00d6d6)',
    themedButton: {
        default: '#00d6d6',
        hover: '#00baba',
        active: '#009191'
    }
}