import styled from 'styled-components'
import { lightgrey, fontfamily, borderColor, pagecolor } from 'StyledComponents/colors'

export const PageContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    font-family: ${fontfamily};
`

export const PageHeader = styled.header`
    width: 100%;
`

export const Information = styled.div`
    width: 100%;
    padding: 5px;
    border-bottom: 1px solid ${lightgrey};
    display: flex;
    align-items: center;
    flex-direction: row;
`

export const AppName = styled.div`
    width: 100%;
    font-weight: 900;
    padding-left: 10px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font-size: 20px;
    color: ${(props) => props.theme.defaultColor};
`

export const HeaderButtonContainer = styled.div`
    height: 100%;
    display: flex;
    padding: 0px 10px 0px 10px;
`

export const WhiteBlanck = styled.div`
    width: 100%;
    height: 100%;
    min-height: 300px;
    padding: 15px;
    background-color: ${pagecolor};
    overflow-y: hidden;
`

export const MenuTab = styled.div`
    width: 100%;
    height: 100%;
    box-shadow: 0px 0px 9px 1px ${borderColor};
    overflow: hidden;
    padding: 7px;
    border-radius: 5px;
    background-color: white;
    overflow-y: auto;
    display: flex;
    flex-direction: column;
`
