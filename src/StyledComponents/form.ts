import styled from 'styled-components'
import { borderColor, button, dangerColor, fontfamily } from 'StyledComponents/colors'

const titlefontsize = '14px'
const labelfontsize = '12px'
const errorfotnsize = '10px'
const headercolor = '#f0f0f0'

export const Form = styled.form`
    width: ${(props) => props.width || '320px'};
    background: #ffffff;
    display: flex;
    flex-direction: column;
    border-radius: 5px;
    border: 0.4px solid ${borderColor};
    font-family: ${fontfamily};
`

export const FormHeader = styled.div`
    font-size: ${titlefontsize};
    font-weight: 700;
    color: ${props => props.theme.defaultColor};
    width: 100%;
    padding: 7.5px;
    text-align: left;
    background-color: ${headercolor};
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    position: relative;
`

export const FormBody = styled.div`
    width: 100%;
    padding: 7.5px;
    display: flex;
    flex-direction: ${(props) => props.direction || 'column'};
`

export const FormFooter = styled.div`
    width: 100%;
    padding: 7.5px;
    display: flex;
    flex-direction: ${(props) => props.direction || 'column'};
`

export const FieldContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    padding: 2px;
`

export const FieldLabel = styled.label`
    font-size: ${labelfontsize};
    padding-bottom: 3px;
    width: 100%;
    text-align: left;
`

export const TextField = styled.input`
    background: #ffffff;
    width: 100%;
    padding: 6px;
    border-radius: 5px;
    border: 1px solid ${(props) => (props.error ? dangerColor : borderColor)};
    outline: 0;

    &:focus {
        border: 1px solid ${(props) => (props.error ? dangerColor : props.theme.defaultColor)};
        box-shadow: 0px 0px 0.1px 1px ${(props) => (props.error ? dangerColor : props.theme.defaultColor)};
    }

    &:disabled {
        opacity: 0.5;
        background: ${borderColor};
    }
`

export const ErrorsContainer = styled.div`
    padding-left: 5px;
    padding-right: 5px;
    width: 100%;
    font-size: ${errorfotnsize};
    color: ${dangerColor};
`

export const FormButtonContainer = styled.div`
    width: 100%;
    padding: 2px;
`

export const FormButton = styled.button`
    ${(props) => props.maxWidth && 'width: 100%;'}
    font-size: 14px;
    line-height: 14px;
    border-radius: 5px;
    ${(props) => ((props.color && props.color !== 'link') || props.themed) && 'font-weight: 600;'}
    ${(props) => ((props.color && props.color !== 'link') || props.themed) && 'padding: 6px 12px 6px 12px;'}
    border: 0;
    outline: 0;
    color: #ffffff;
    background-color: ${(props) => !props.themed ? (button[props.color && button[props.color] ? props.color : 'primary'].default) : props.theme.themedButton.default};
    ${(props) => props.color && props.color !== 'link' && 'cursor: pointer;'}
    transition: all 0.2s ease;
    position: relative;
    cursor: pointer;

    & > a {
        color: ${button.primary.default};
        text-decoration: none;
        cursor: pointer;
    }

    &:hover {
        background-color: ${(props) => !props.themed ? (button[props.color && button[props.color] ? props.color : 'primary'].hover) : props.theme.themedButton.hover};
    }

    &:active {
        background-color: ${(props) => !props.themed ? (button[props.color && button[props.color] ? props.color : 'primary'].active) : props.theme.themedButton.active};
    }

    ${props => !!props.notifications && (
        `
            &:before {
                position: absolute;
                top: -4px;
                right: -3px;
                border-radius: 15px;
                background: red;
                padding: 0px 4px 0px 4px;
                font-size: 12px;
                font-weight: 900;
                content: '${props.notifications}';
            }
        `
    )}
`