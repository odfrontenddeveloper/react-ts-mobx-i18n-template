import styled from 'styled-components'
import { fontfamily } from 'StyledComponents/colors'

export const ModalContainer = styled.div`
    top: 0;
    left: 0;
    position: fixed;
    z-index: 70;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    padding: 25px;
`

export const ModalClickOutside = styled.div`
    top: 0;
    left: 0;
    position: absolute;
    z-index: 80;
    background-color: rgba(0, 0, 0, 0.75);
    width: 100%;
    height: 100%;
`

export const ModalContent = styled.div`
    position: relative;
    z-index: 90;
`

export const CloseModalButton = styled.button`
    transform: scale(1.3);
    margin: 0;
    padding: 0;
    outline: 0;
    background: 0;
    border: 0;
    cursor: pointer;
    color: white;
    position: absolute;
    top: -10px;
    right: -20px;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const Waiting = styled.div`
    border-radius: 5px;
    font-family: ${fontfamily};
    padding: 20px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: white;
`
