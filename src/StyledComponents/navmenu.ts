import styled from 'styled-components'
import { dark, fontfamily, grey, lightgrey, transition, white } from 'StyledComponents/colors'

export const NavMenu = styled.nav`
    width: 100%;
    display: flex;
    padding: 5px;
    flex-direction: row;
    justify-content: center;
    border-bottom: 1px solid ${lightgrey};
    flex-wrap: wrap;
    font-family: ${fontfamily};
`

export const NavItemContainer = styled.div`
    padding: 1px 4px 1px 4px;
    text-decoration: none;
    font-family: ${fontfamily};
`

export const NavItem = styled.button`
    border: 0;
    outline: 0;
    padding: 7px 10px 7px 10px;
    border-radius: 5px;
    font-size: 14px;
    font-weight: 600;
    background-color: ${props => props.selected ? props.theme.defaultColor : lightgrey};
    color: ${props => props.selected ? white : dark};
    cursor: pointer;
    transition: ${transition};
    font-family: ${fontfamily};

    ${props => !props.selected && (
        `
        &:hover {
            background-color: ${grey};
        }
        `
    )}
`