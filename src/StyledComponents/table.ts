import styled from 'styled-components'
import { borderColor, dark, fontfamily, tableHeaderColor, transition, white } from 'StyledComponents/colors'

export const TablePage = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    overflow: hidden;
`

export const TableHeader = styled.div`
    width: 100%;
    padding: 5px;
`

export const SearchContainer = styled.div`
    padding: 1.5px 10px 1.5px 10px;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    overflow-x: auto;
`

export const SearchContainerText = styled.div`
    border-bottom-left-radius: 5px;
    border-top-left-radius: 5px;
    background: ${props => props.theme.defaultColor};
    color: ${white};
    display: flex;
    align-items: center;
    font-size: 14px;
    font-weight: 700;
    padding: 5px 10px 5px 10px;
`

export const SearchContainerSelect = styled.select`
    width: 150px;
    height: 100%;
    outline: 0;
    padding: 5px;
    border: 1px solid ${props => props.theme.defaultColor};
    border-right: 0;
    color: ${dark};
`

export const SortContainerSelect = styled.select`
    width: 150px;
    height: 100%;
    outline: 0;
    border-radius: 0;
    padding: 5px;
    border: 1px solid ${props => props.theme.defaultColor};
    border-left: 0;
    color: ${dark};
`

export const SearchContainerInput = styled.input`
    background: white;
    width: 120px;
    height: 100%;
    outline: 0;
    padding: 5px;
    border: 1px solid ${props => props.theme.defaultColor};
    border-left: 0;
    color: ${dark};
`

export const PaginateContainer = styled.div`
    background: ${borderColor};
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    width: 100%;
    height: auto;
    padding: 5px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
`

const height = '100%'
const thumbHeight = '100%'

export const PaginateControl = styled.div`
    padding-right: 10px;
    display: flex;
    flex-direction: row;
`

export const PaginateButton = styled.button`
    height: 100%;
    border: 0;
    margin: 0;
    padding: 5px 10px 5px 10px;
    background: ${white};
    outline: 0;
    background-color: ${(props) => props.theme.defaultColor};
    color: ${white};
    ${(props) => props.leftButton && 'border-bottom-left-radius: 5px;'};
    ${(props) => props.leftButton && 'border-top-left-radius: 5px;'};
    ${(props) => props.rightButton && 'border-bottom-right-radius: 5px;'};
    ${(props) => props.rightButton && 'border-top-right-radius: 5px;'};
    transition: ${transition};
    cursor: pointer;

    &:hover {
        background-color: ${(props) => props.theme.themedButton.hover};
    }

    &:active {
        background-color: ${(props) => props.theme.themedButton.active};
    }
`

export const PaginateControlLabel = styled.div`
    padding: 5px;
    display: flex;
    align-items: center;
    border: 1px solid ${props => props.theme.defaultColor};
    background-color: ${white};
    ${(props) => props.leftRadius && 'border-bottom-left-radius: 5px;'};
    ${(props) => props.leftRadius && 'border-top-left-radius: 5px;'};
`

export const Wrapper = styled.div`
    padding: 5px;
    min-width: 100px;
    width: 200px;
    background: ${borderColor};
    border-radius: 5px;
`

export const Range = styled.input`
    /* border-radius: 5px; */
    overflow: hidden;
    display: block;
    appearance: none;
    width: 100%;
    margin: 0;
    height: ${height};
    cursor: pointer;

    &:focus {
        outline: none;
    }

    &::-webkit-slider-runnable-track {
        width: 100%;
        height: ${height};
        /* border-radius: 5px; */
        background: ${white};
        border: 1px solid ${borderColor};
    }

    &::-webkit-slider-thumb {
        position: relative;
        appearance: none;
        height: ${thumbHeight};
        width: ${(props) => props.thumbWidth || '10px'};
        background: ${(props) => props.theme.defaultColor};
        border: 0;
        /* border-radius: 5px; */
        top: 50%;
        transform: translateY(-50%);
        transition: background-color 150ms;
    }

    &::-moz-range-track,
    &::-moz-range-progress {
        width: 100%;
        height: ${height};
        background: ${white};
        /* border-radius: 5px; */
        border: 1px solid ${borderColor};
    }

    &::-moz-range-progress {
        background: ${white};
        /* border-radius: 5px; */
        border: 1px solid ${borderColor};
    }

    &::-moz-range-thumb {
        appearance: none;
        margin: 0;
        height: ${thumbHeight};
        width: ${(props) => props.thumbWidth || '10px'};
        background: ${(props) => props.theme.defaultColor};
        border: 0;
        /* border-radius: 5px; */
        border: 0;
        transition: background-color 150ms;
    }

    &::-ms-track {
        width: 100%;
        height: ${height};
        border: 0;
        /* color needed to hide track marks */
        color: transparent;
        background: transparent;
    }

    &::-ms-fill-lower {
        background: ${white};
    }

    &::-ms-fill-upper {
        background: ${white};
    }

    &::-ms-thumb {
        appearance: none;
        height: ${thumbHeight};
        width: ${(props) => props.thumbWidth || '10px'};
        background: ${(props) => props.theme.defaultColor};
        /* border-radius: 5px; */
        border: 0;
        transition: background-color 150ms;
        /* IE Edge thinks it can support -webkit prefixes */
        top: 0;
        margin: 0;
        box-shadow: none;
    }

    &:hover,
    &:focus {
        &::-webkit-slider-thumb {
            background: ${(props) => props.theme.defaultColor};
        }
        &::-moz-range-thumb {
            background: ${(props) => props.theme.defaultColor};
        }
        &::-ms-thumb {
            background: ${(props) => props.theme.defaultColor};
        }
    }
`

export const TableContainer = styled.div`
    width: 100%;
    height: 100%;
    border: 1px solid ${borderColor};
    overflow: auto;
`

export const Table = styled.table`
    width: 100%;
    font-family: ${fontfamily};
`

export const THead = styled.thead``

export const TBody = styled.tbody`
    height: 100%;
`

export const Tr = styled.tr`
    border-bottom: 1px solid silver;
`

export const Td = styled.td`
    text-align: center;
    ${(props) => (props.buttonContainer ? '' : 'vertical-align: bottom;')}
    ${(props) => (props.themed && `background-color: ${props.theme.defaultColorOpacity};`)}
    position: relative;

    ${props => props.themed && (`
        &:after {
            position: absolute;
            content: '';
            top: 0;
            left: 100%;
            background-image: linear-gradient(to top left, rgba(0, 0, 0, 0) 50%, ${props.theme.defaultColorOpacity} 50%);
            height: 100%;
            width: 15px;
        }
    `)}
`

export const IndexContainer = styled.div`
    min-height: 40px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    text-align: center;
    padding: 5px;
`

export const Index = styled.div`
    color: ${(props) => props.theme.defaultColor};
    font-weight: 900;
    text-align: center;
    height: 100%;
    width: 100%;
    padding: 0px 7px 0px 7px;
    font-size: 14px;
`

export const TdContentContainer = styled.div`
    min-height: 40px;
    display: flex;
    flex-direction: column;
    text-align: center;
    padding: 5px 20px 5px 20px;
`

export const TdContentHeader = styled.div`
    white-space: nowrap;
    text-align: center;
    width: 100%;
    font-size: 11px;
    color: ${tableHeaderColor};
`

export const TdContentValue = styled.div`
    white-space: nowrap;
    text-align: center;
    width: 100%;
    font-size: 14px;
`

export const TdButtonContainer = styled.div`
    height: 40px;
    display: flex;
    padding: 5px;
`
