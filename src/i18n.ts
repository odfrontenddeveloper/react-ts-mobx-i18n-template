import i18n from 'i18next'

import datatableEN from 'locales/en/datatable.json'
import datatableRU from 'locales/ru/datatable.json'
import datatableUK from 'locales/uk/datatable.json'

import validationEN from 'locales/en/validation.json'
import validationRU from 'locales/ru/validation.json'
import validationUK from 'locales/uk/validation.json'

import loginEN from 'locales/en/login.json'
import loginRU from 'locales/ru/login.json'
import loginUK from 'locales/uk/login.json'

import newuserEN from 'locales/en/newuser.json'
import newuserRU from 'locales/ru/newuser.json'
import newuserUK from 'locales/uk/newuser.json'

import { initReactI18next } from 'react-i18next'
import detector from 'i18next-browser-languagedetector'

i18n.use(initReactI18next)
    .use(detector)
    .init({
        resources: {
            en: {
                translation: {
                    datatable: datatableEN,
                    validation: validationEN,
                    login: loginEN,
                    newuser: newuserEN,
                },
            },
            ru: {
                translation: {
                    datatable: datatableRU,
                    validation: validationRU,
                    login: loginRU,
                    newuser: newuserRU,
                },
            },
            uk: {
                translation: {
                    datatable: datatableUK,
                    validation: validationUK,
                    login: loginUK,
                    newuser: newuserUK,
                },
            },
        },
        lng: 'en',
        fallbackLng: 'en',
        interpolation: {
            escapeValue: false,
        },
    })

export default i18n
