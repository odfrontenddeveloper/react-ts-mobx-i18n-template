export const minlength5 = (value) => (!!value && value.length >= 5 ? false : 'minlength5')
export const maxlength30 = (value) =>
    !!value && value.length <= 30 ? false : 'maxlength30'
export const maxlength45 = (value) =>
    !!value && value.length <= 45 ? false : 'maxlength45'
export const number = (value) => (!!value && value.length && !isNaN(+value) ? false : 'number')
export const required = (value) => (!!value ? false : 'required')
export const email = (value) =>
    !!value &&
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        String(value).toLocaleLowerCase()
    )
        ? false
        : 'email'

const validations = {
    minlength5,
    maxlength30,
    maxlength45,
    number,
    required,
    email,
}

export default validations
