import { BrowserRouter as Router, Route } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import Login from 'Containers/Login/Login'
import NewUser from 'Containers/Login/NewUser'
import OwnerPage from 'Containers/OwnerPage/OwnerPage'
import DeleteModal from 'BuildComponents/DeleteModal'
import { blueTheme } from 'StyledComponents/theme'
import 'App.css'
import i18n from 'i18n'

const findCurrentLng: string | null = localStorage.getItem('lng')

if(findCurrentLng && findCurrentLng !== i18n.language){
    i18n.changeLanguage(findCurrentLng)
}

const App = () => (
    <ThemeProvider theme={blueTheme}>
        <Router>
            <Route path="/login/new" component={NewUser} />
            <Route path="/login/auth" component={Login} />
            <Route path="/home/owner" component={OwnerPage} />
            <Route path="/home/admin" />
            <Route path="/home/teacher" />
            <Route path="/home/student" />
        </Router>
        <DeleteModal />
    </ThemeProvider>
)

export default App
